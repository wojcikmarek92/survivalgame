﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayNightSystem : MonoBehaviour {

    public Transform sun;
    public Transform center;
    public float dayCycleInMinutes;

    private const float SECOND = 1;
    private const float MINUTE = 60 * SECOND;
    private const float HOUR = 60 * MINUTE;
    private const float DAY = 24 * HOUR;

    public float DEGREES_PER_SECOND = 360 / DAY;

    public float _degreeRotation;

    private float _timeOfDay;
    private float timer = 0.0f;

	// Use this for initialization
	void Start () {        
        _timeOfDay = 0;
        dayCycleInMinutes = 15.0f;
        _degreeRotation = DEGREES_PER_SECOND * DAY / (dayCycleInMinutes * MINUTE);        
    }
	
	// Update is called once per frame
	void Update ()
    {
        RotateSun(CalculateDegree());
        _timeOfDay += Time.deltaTime;
    }

    private void RotateSun(float rotateDegree)
    {
        sun.Rotate(new Vector3(rotateDegree, 0, 0));
    }

    private float CalculateDegree()
    {
        float degree = _degreeRotation * Time.deltaTime;
        return degree;
    }
}
