﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour {
    //hunger
    public float MaxHunger { get; set; }
    private float _minimumHunger = 0.00f;
    public float MinimumHunger {
        get { return _minimumHunger; }
    }
    public float CurrentHunger { get; set; }
    public float DecayHungerFactor { get; set; }
    public float HungerDamageFactor { get; set; }
    public bool IsHungry { get; set; }

    //health
    public float MaxHealth { get; set; }
    private float _minimumHealth = 0.00f;
    public float MinimumHealth
    {
        get { return _minimumHealth; }
    }
    public float CurrentHealth { get; set; }
    public float RegenerateHealthFactor { get; set; }

    //Poison
    public bool IsPoisoned { get; set; }
    public float PoisonDamageFactor { get; set; }
    public float PoisonDurationTime { get; set; }
    public float PoisonHungerFactor { get; set; }

    //Bleeding
    public bool IsBleeding { get; set; }
    public float BleedingDamageFactor { get; set; }
    public float BleedingDurationTime { get; set; }

    // Use this for initialization
    void Start() {
        MaxHunger = 100.0f;        
        CurrentHunger = MaxHunger;
        DecayHungerFactor = 0.05f;
        IsHungry = false;
        HungerDamageFactor = 0.1f;

        MaxHunger = 100.0f;        
        CurrentHealth = MaxHealth;
        RegenerateHealthFactor = 0.05f;        

        IsPoisoned = false;
        PoisonDamageFactor = 0.0f;
        PoisonDurationTime = 0.0f;
        PoisonHungerFactor = 0.0f;

        IsBleeding = false;
        BleedingDamageFactor = 0.0f;
        BleedingDurationTime = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        DecayHunger(DecayHungerFactor);        
        RegenerateHealth(RegenerateHealthFactor);        
        UpdateStatusHealth();
        Debug.Log(CurrentHunger);
    }

    //Public Methods
       
    public void AddHealth(float amountToAdd)
    {
        CurrentHealth += amountToAdd;
        CurrentHealth = Mathf.Clamp(CurrentHealth, MinimumHealth, MaxHealth);
    }

    public void RemoveHealth(float amountToRemove)
    {
        CurrentHealth -= amountToRemove;
        CurrentHealth = Mathf.Clamp(CurrentHealth, MinimumHealth, MaxHealth);
        if(CurrentHealth == MinimumHealth)
        {
            Debug.Log("Zdechles z ran"); //TODO implementacja zdechniecia
        }
    }

    public void GivePoison(float poisonDurationTime, float poisonDamageFactor, float poisonHungerFactor)
    {
        IsPoisoned = true;
        PoisonDurationTime = poisonDurationTime;
        PoisonDamageFactor = poisonDurationTime;
    }

    public void GiveBleeding(float bleedingDurationTime, float bleedingDamageFactor)
    {
        IsBleeding = true;
        BleedingDurationTime = bleedingDurationTime;
        BleedingDamageFactor = bleedingDurationTime;
    }

    public void CurePoison()
    {
        if (IsPoisoned)
            RemovePoison();
    }

    public void CureBleeding()
    {
        if (IsBleeding)
            RemoveBleeding();
    }

    public void AddHunger(float amountToAdd)
    {
        CurrentHunger += amountToAdd;
        CurrentHunger = Mathf.Clamp(CurrentHunger, MinimumHunger, MaxHunger);
    }

        // Private Methods

    private void RemovePoison()
    {
        IsPoisoned = !IsPoisoned;
        PoisonDurationTime = 0.0f;
        PoisonDamageFactor = 0.0f;
    }

    private void RemoveBleeding()
    {
        IsBleeding = !IsBleeding;
        BleedingDurationTime = 0.0f;
        BleedingDamageFactor = 0.0f;
    }

    private void CheckIfHungry()
    {
        if (CurrentHunger <= MinimumHunger)
            IsHungry = true;
        else
            IsHungry = false;
        
    }

    private void UpdateStatusHealth()
    {
        if (IsPoisoned)
        {
            if (PoisonDurationTime <= 0.0f)
            {
                RemovePoison();
            }
            else
            {
                PoisonDurationTime -= Time.deltaTime;
                RemoveHealth(PoisonDamageFactor * Time.deltaTime);
            }
        }
        if (IsBleeding)
        {
            if (BleedingDurationTime <= 0.0f)
            {
                RemoveBleeding();
            }
            else
            {
                BleedingDurationTime -= Time.deltaTime;
                RemoveHealth(BleedingDamageFactor * Time.deltaTime);
            }
        }
        if (IsHungry)
        {
            RemoveHealth(HungerDamageFactor * Time.deltaTime);
        }
    }
       

    private void RegenerateHealth(float regenerateFactor)
    {
        if (!IsPoisoned && !IsBleeding)
        {
            CurrentHealth += CurrentHunger / MaxHunger * regenerateFactor * Time.deltaTime;
            CurrentHealth = Mathf.Clamp(CurrentHealth, MinimumHealth, MaxHealth);
        }
    }    

    private void DecayHunger (float decayFactor)
    {
        CurrentHunger -= decayFactor * Time.deltaTime;
        CurrentHunger = Mathf.Clamp(CurrentHunger, MinimumHunger, MaxHunger);
        if(CurrentHunger == MinimumHunger)
        {
            Debug.Log("Zdychasz z glodu"); //TODO obsluga zdychania z glodu.
        }
        CheckIfHungry();
    }

    
}
