﻿using UnityEngine;
using System.Collections;

public class Foundation : Item
{
    /// <summary>
    /// The amount of health the item will restore
    /// </summary>
   

    /// <summary>
    /// The amount of mana, the item will restore
    /// </summary>
 

    /// <summary>
    /// Empty constructor used for serializing the object
    /// </summary>
    public Foundation()
    { }

    /// <summary>
    /// The consumeable's constructor
    /// </summary>
    /// <param name="itemName">The name of the item</param>
    /// <param name="description">The item's description</param>
    /// <param name="itemType">The type of time</param>
    /// <param name="quality">The item's quality</param>
    /// <param name="spriteNeutral">Path to the item's sprite</param>
    /// <param name="spriteHighlighted">Path to the items highlighted sprite</param>
    /// <param name="maxSize">The item's max size</param>
    /// <param name="health">The amount of health the item will restore</param>
    /// <param name="mana">The amount of mana the item will restore</param>
	public Foundation(string itemName, string description, ItemType itemType, Quality quality,  string spriteNeutral, string spriteHighlighted, int maxSize)
        : base(itemName, description, itemType, quality, spriteNeutral, spriteHighlighted, maxSize)
    {
       
    }

    /// <summary>
    /// Uses the item
    /// </summary>
    //tutaj uzycie podlogi
    public override void Use(Slot slot, ItemScript item)
    {
        Debug.Log("Used foundation " + ItemName);
        slot.RemoveItem();
    }

    /// <summary>
    /// Creates a tooltip
    /// </summary>
    /// <returns>A complete tooltip</returns>
    public override string GetTooltip(Inventory inv)
    {
        string stats = string.Empty;


        //Gets the tooltip from the base class
        string itemTip = base.GetTooltip(inv);

        //Returns the complete tooltip

        if (inv is VendorInventory)
        {
            return string.Format("{0}" + "<size=14>{1}\n<color=yellow>Price: {2}</color></size>", itemTip, stats,BuyPrice);
        }
        else if (VendorInventory.Instance.IsOpen)
        {
            return string.Format("{0}" + "<size=14>{1}\n<color=yellow>Price: {2}</color></size>", itemTip, stats, SellPrice);
        }
        else
        {
            return string.Format("{0}" + "<size=14>{1}</size>", itemTip, stats);
        }
        
    }
}
